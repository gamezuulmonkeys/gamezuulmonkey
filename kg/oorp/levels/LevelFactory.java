package kg.oorp.levels;

import kg.oorp.items.Key;
import kg.oorp.rooms.Room;
import kg.oorp.rooms.RoomFactory;
import kg.oorp.units.Monster;
import kg.oorp.units.MonsterFactory;
import kg.oorp.units.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by murat on 12/5/16.
 */
public class LevelFactory {
    private static final Integer LEVEL_ONE = 1;
    private static final Integer LEVEL_TWO = 2;
    private static final Integer LEVEL_THREE = 3;
    private static final Integer FIRST_ROOM_NUMBER = 0;
    private static LevelFactory ourInstance = new LevelFactory();
    private ThreadGroup monsterThreadGroup;
    private Random random;

    private LevelFactory() {
        monsterThreadGroup = new ThreadGroup("Monsters");
        random = new Random();
    }

    public static LevelFactory getOurInstance() {
        return ourInstance;
    }

    public Level getLevelOne() {
        Level level = new Level(LEVEL_ONE);
        List<Room> rooms = initRoomsLevelOne();
        initUnitsLevelOne(rooms);
        level.setRooms(rooms);
        return level;
    }

    public Level getLevelTwo() {
        Level level = new Level(LEVEL_TWO);
        List<Room> rooms = initRoomsLevelTwo();
        initUnitsLevelTwo(rooms);
        level.setRooms(rooms);
        return level;
    }

    public Level getLevelThree() {
        Level level = new Level(LEVEL_THREE);
        List<Room> rooms = initRoomsLevelTwo();
        initUnitsLevelThree(rooms);
        level.setRooms(rooms);
        return level;
    }

    private List<Room> initRoomsLevelTwo() {
        List<Room> rooms = new ArrayList<>();
        Room livingRoom = RoomFactory.getInstance().getLivingRoom();
        Room cabinet = RoomFactory.getInstance().getCabinet();
        Room canteen = RoomFactory.getInstance().getCanteen();
        Room hall = RoomFactory.getInstance().getHall();
        hall.setOpened(false);
        hall.addAvailableRooms(livingRoom, cabinet, canteen);
        canteen.setExit(true);
        livingRoom.addItems(new Key(hall.getKeyValue()));
        rooms.add(livingRoom);
        rooms.add(cabinet);
        rooms.add(canteen);
        rooms.add(hall);
        return rooms;
    }

    private List<Room> initRoomsLevelOne() {
        List<Room> rooms = new ArrayList<>();
        Room bedroom = RoomFactory.getInstance().getBedroom();
        Room bathroom = RoomFactory.getInstance().getBathroom();
        Room canteen = RoomFactory.getInstance().getCanteen();
        rooms.add(bedroom);
        rooms.add(bathroom);
        rooms.add(canteen);
        canteen.setExit(true);
        canteen.setOpened(false);
        bedroom.addItems(new Key(canteen.getKeyValue()));
        bedroom.addAvailableRooms(bathroom, canteen);
        return rooms;
    }

    private void initUnitsLevelOne(List<Room> rooms) {
        Monster zombie = MonsterFactory.getInstance().newZombie();
        zombie.goRoom(rooms.get(random.nextInt(rooms.size())));
        new Thread(monsterThreadGroup, zombie).start();
        Player.getInstance().goRoom(rooms.get(FIRST_ROOM_NUMBER));
    }

    private void initUnitsLevelTwo(List<Room> rooms) {
        Monster badGuy = MonsterFactory.getInstance().newBadGuy();
        badGuy.goRoom(rooms.get(random.nextInt(rooms.size())));
        new Thread(monsterThreadGroup, badGuy).start();
        Player.getInstance().goRoom(rooms.get(FIRST_ROOM_NUMBER));
    }

    private void initUnitsLevelThree(List<Room> rooms) {
        Monster badGuy = MonsterFactory.getInstance().newBadGuy();
        Monster shrek = MonsterFactory.getInstance().newShreck();
        badGuy.goRoom(rooms.get(random.nextInt(rooms.size())));
        shrek.goRoom(rooms.get(random.nextInt(rooms.size())));
        new Thread(monsterThreadGroup, badGuy).start();
        new Thread(monsterThreadGroup, shrek).start();
        Player.getInstance().goRoom(rooms.get(FIRST_ROOM_NUMBER));
    }

    public ThreadGroup getMonsterThreadGroup() {
        return monsterThreadGroup;
    }
}
