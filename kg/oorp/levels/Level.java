package kg.oorp.levels;

import kg.oorp.commands.Parser;
import kg.oorp.rooms.Room;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Fujitsu on 30.11.2016.
 */
public class Level {
    private Integer level;
    private List<Room> rooms;

    public Level(Integer level) {
        this.level = level;
        rooms = new ArrayList<>();
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public void addRoom(Room... rooms) {
        this.rooms.addAll(Arrays.asList(rooms));
    }

    public void removeRoom(Room... rooms) {
        this.rooms.removeAll(Arrays.asList(rooms));
    }


    public void startGame() {
        new Parser().startConsoleReading();
    }
}
