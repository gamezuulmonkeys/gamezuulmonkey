package kg.oorp.items;

public interface Item {
    Integer getPower();

    String getName();
}
