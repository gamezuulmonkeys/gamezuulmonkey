package kg.oorp.items;


public enum Chest implements Item {

    GUN(10), SWORD(5), KNIFE(3), FLOWER(0), PILL(0);

    private final Integer power;

    Chest(Integer power) {
        this.power = power;
    }

    public static Integer size() {
        return Chest.values().length;
    }

    @Override
    public Integer getPower() {
        return power;
    }

    public String getName() {
        return String.valueOf(this);
    }
}
