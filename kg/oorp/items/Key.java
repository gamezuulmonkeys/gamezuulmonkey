package kg.oorp.items;

public class Key implements Item {
    private Integer keyValue;

    public Key(Integer keyValue) {
        this.keyValue = keyValue;
    }

    public Integer getKeyValue() {
        return keyValue;
    }

    @Override
    public Integer getPower() {
        return 0;
    }

    public String getName() {
        return getClass().getSimpleName();
    }
}
