package kg.oorp.units;

import kg.oorp.rooms.Room;

/**
 * Created by Fujitsu on 17.11.2016.
 */
public interface Unit {
    String getName();

    void attack(Unit unit);

    void goRoom(Room room);

    Integer getHealth();

    void setHealth(Integer health);
}
