package kg.oorp.units;

/**
 * Created by ToughGuy on 11.12.2016.
 */
public class MonsterFactory {

    private static MonsterFactory ourInstance = new MonsterFactory();

    private MonsterFactory() {
    }

    public static MonsterFactory getInstance() {
        return ourInstance;
    }

    public Monster newShreck() {
        return new Monster("Shrek", 10, 40);
    }

    public Monster newBadGuy() {
        return new Monster("BadGuy", 5, 25);
    }

    public Monster newZombie() {
        return new Monster("Zombie", 10, 30);
    }

}
