package kg.oorp.units;

import kg.oorp.items.Item;
import kg.oorp.items.Chest;
import kg.oorp.items.Key;
import kg.oorp.rooms.Room;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rizat on 11.12.2016.
 */

public class Player implements Unit {
    public static final String NAME = "Player";
    public static final Integer MAX_HEALTH = 100;
    public static final Integer MIN_HEALTH = 0;
    public static final Integer POWER = 5;
    private static Player ourInstance = new Player();
    private static Integer health;
    private Integer summaryPower;
    private Room currentRoom;
    private List<Item> inventory;
    private Item itemOnHand;

    private Player() {
        inventory = new ArrayList<>();
        health = Integer.valueOf(MAX_HEALTH);
        summaryPower = Integer.valueOf(POWER);
    }

    public static Player getInstance() {
        return ourInstance;
    }

    @Override
    public void attack(Unit unit) {
        if (unit.getHealth() > MIN_HEALTH)
            unit.setHealth(unit.getHealth() - summaryPower);
    }

    @Override
    public void goRoom(Room room) {
        if (room.isOpened()) {
            Room previousRoom = currentRoom;
            currentRoom = room;
            room.addUnits(this);
            if (previousRoom != null) {
                previousRoom.removeUnit(this);
            }
            return;
        }
        if (canOpen(room)) {
            room.setOpened(true);
            goRoom(room);
        }
    }

    public Room goRoom(String targetRoom) {
        for (Room room : currentRoom.getAvailableRooms()) {
            if (room.getName().equalsIgnoreCase(targetRoom)) {
                goRoom(room);
                return room;
            }
        }
        return null;
    }

    private Boolean canOpen(Room room) {
        for (Item item : inventory) {
            if (item instanceof Key && ((Key) item).getKeyValue().equals(room.getKeyValue())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Integer getHealth() {
        return health;
    }

    @Override
    public void setHealth(Integer health) {
        Player.health = health;
    }

    public void heal() {
        if (inventory.contains(Chest.PILL)) {
            health = MAX_HEALTH;
            inventory.remove(Chest.PILL);
        }
    }

    private void addToInventory(Item item) {
        inventory.add(item);
        currentRoom.removeItem(item);
    }

    public Item addToInventory(String itemName) {
        for (Item item : currentRoom.getItems()) {
            if (item.getName().equalsIgnoreCase(itemName)) {
                addToInventory(item);
                return item;
            }
        }
        return null;
    }

    private void removeFromInventory(Item item) {
        inventory.remove(item);
        currentRoom.addItems(item);
    }

    public Item removeFromInventory(String itemName) {
        for (Item item : inventory) {
            if (item.getName().equalsIgnoreCase(itemName)) {
                removeFromInventory(item);
                return item;
            }
        }
        return null;
    }

    public List<Item> getInventory() {
        return inventory;
    }

    public Item selectItemOnHand(Integer index) {
        if (index <= inventory.size() && index > 0) {
            itemOnHand = inventory.get(index - 1);
            summaryPower = POWER + itemOnHand.getPower();
            return itemOnHand;
        }
        return null;
    }

    public Item getItemOnHand() {
        return itemOnHand;
    }

    public String inventoryShow() {
        String things = "";
        for (Item item : inventory) {
            things += "\n" + item.getName();
        }
        return things;
    }

    public Unit getUnderAttackUnit(String unitName) {
        for (Unit unit : currentRoom.getUnits()) {
            if (unit.getName().equalsIgnoreCase(unitName))
                return unit;
        }
        return new Monster("", 0, 0);
    }

    public Room getCurrentRoom() {
        return currentRoom;
    }

    public String getName() {
        return NAME;
    }

}
