package kg.oorp.units;

import kg.oorp.commands.Parser;
import kg.oorp.rooms.Room;
import kg.oorp.services.DefaultConsoleIOService;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Monster implements Unit, Runnable {
    private static final Integer MIN_HEALTH = 0;
    private static final Integer ATTACK_TIME = 10000;
    private static final Integer CHANGE_ROOM_TIME = 30000;
    private String name;
    private Integer health;
    private Integer power;
    private Room currentRoom;
    private Random random;

    public Monster(String name, Integer power, Integer health) {
        this.name = name;
        this.power = power;
        random = new Random();
        this.health = health;
    }

    @Override
    public void attack(Unit unit) {
        if (unit.getHealth() > MIN_HEALTH) {
            unit.setHealth(unit.getHealth() - power);
        }
    }

    @Override
    public void goRoom(Room room) {
        Room previousRoom = currentRoom;
        currentRoom = room;
        room.addUnits(this);
        if (null != previousRoom) {
            previousRoom.removeUnit(this);
        }
    }

    @Override
    public Integer getHealth() {
        return health;
    }

    @Override
    public void setHealth(Integer health) {
        this.health = health;
    }

    private void startMonster() throws InterruptedException {
        while (health > MIN_HEALTH) {
            if (currentRoom.getUnits().contains(Player.getInstance()) && Player.getInstance().getHealth() > MIN_HEALTH) {
                attack(Player.getInstance());
                new DefaultConsoleIOService().write(currentRoom.getUnitsLifeAsString());
                Thread.sleep(ATTACK_TIME);
                continue;
            }
            if (Player.getInstance().getHealth() <= Player.MIN_HEALTH) {
                stopGame();
            }
            if (!currentRoom.getUnits().contains(Player.getInstance())) {
                goRandomRoom();
                Thread.sleep(random.nextInt(CHANGE_ROOM_TIME));
            }
        }
        currentRoom.removeUnit(this);
    }

    private void goRandomRoom() {
        List<Room> rooms = new ArrayList<>();
        rooms.addAll(currentRoom.getAvailableRooms());
        goRoom(rooms.get(random.nextInt(rooms.size())));
    }


    public String getName() {
        return name;
    }

    private void stopGame() {
        new DefaultConsoleIOService().write(Parser.GAME_OVER);
        System.exit(Parser.EXIT_CODE);
    }

    @Override
    public void run() {
        try {
            startMonster();
        } catch (InterruptedException e) {
            run();
        }
    }

}
