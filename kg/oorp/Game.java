package kg.oorp;

import kg.oorp.levels.LevelFactory;

public class Game {
    public static void main(String[] args) {
        LevelFactory.getOurInstance().getLevelThree().startGame();
    }
}
