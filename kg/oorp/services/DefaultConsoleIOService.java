package kg.oorp.services;

import java.util.Scanner;

/**
 * Created by Aibek on 12.11.2016.
 */
public class DefaultConsoleIOService implements IOService {


    @Override
    public void write(String message) {
        System.out.println("/////////////////////////////////////////");
        System.out.println(message);
        System.out.println("/////////////////////////////////////////");
    }

    @Override
    public String scan() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    @Override
    public Integer scanInt() {
        Scanner scanner = new Scanner(System.in);
        Integer input;
        try {
            input = scanner.nextInt();
        } catch (Exception e) {
            return 0;
        }
        scanner.close();
        return input;
    }
}
