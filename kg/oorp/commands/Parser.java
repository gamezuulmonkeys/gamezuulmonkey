package kg.oorp.commands;

import kg.oorp.items.Item;
import kg.oorp.levels.LevelFactory;
import kg.oorp.units.Player;
import kg.oorp.services.DefaultConsoleIOService;
import kg.oorp.services.IOService;

/**
 * Created by Fujitsu on 23.11.2016.
 */
public class Parser {
    public static final String GAME_OVER = "GAME OVER";
    public static final Integer EXIT_CODE = 0;
    private static final String GREAT_MESSAGE = "Welcome to game World of Zuul\n" +
            "please type one of the following commands:\n";
    private static final String SLOT_EMPTY_MESSAGE = "Slot is empty!";
    private static final Integer COMMAND_LENGTH = 2;
    private IOService ioService;
    private CommandSet commandSet;

    public Parser() {
        ioService = new DefaultConsoleIOService();
        commandSet = CommandSet.getInstance();
    }

    private Command getCommandFromConsole() {
        String[] inputString = ioService.scan().split("[\\s]+", COMMAND_LENGTH);
        if (inputString.length > 0 && commandSet.isCommand(inputString[0])) {
            String firstWord = inputString[0];
            String secondWord = (inputString.length == COMMAND_LENGTH) ? inputString[1] : " ";
            return new Command(firstWord, secondWord);
        }
        return new Command("", "");
    }

    private void doCommand(Command command) {
        Player player = Player.getInstance();
        if (command.getFirstWord().equalsIgnoreCase(CommandSet.HELP)) {
            ioService.write(greatMessage());
            return;
        }
        if (command.getFirstWord().equalsIgnoreCase(CommandSet.GO)) {
            player.goRoom(command.getSecondWord());
            ioService.write(player.getCurrentRoom().getDescription());
            return;
        }
        if (command.getFirstWord().equalsIgnoreCase(CommandSet.LOOK)) {
            ioService.write(player.getCurrentRoom().getDescription());
            return;
        }
        if (command.getFirstWord().equalsIgnoreCase(CommandSet.TAKE)) {
            Item item = player.addToInventory(command.getSecondWord());
            player.selectItemOnHand(player.getInventory().indexOf(item)+1);
            ioService.write(item.getName());
            return;
        }
        if (command.getFirstWord().equalsIgnoreCase(CommandSet.DROP)) {
            Item item = player.removeFromInventory(command.getSecondWord());
            ioService.write(item.getName());
            return;
        }
        if (command.getFirstWord().equalsIgnoreCase(CommandSet.INVENTORY)) {
            ioService.write(player.inventoryShow());
            return;
        }
        if (command.getFirstWord().equalsIgnoreCase(CommandSet.HEAL)) {
            player.heal();
            return;
        }
        if (command.getFirstWord().equalsIgnoreCase(CommandSet.SELECT_ITEM)) {
            try {
                player.selectItemOnHand(Integer.parseInt(command.getSecondWord()));
                ioService.write(String.valueOf(player.getItemOnHand().getName()));
            } catch (NumberFormatException e) {
                ioService.write(SLOT_EMPTY_MESSAGE);
            }
        }
        if (command.getFirstWord().equalsIgnoreCase(CommandSet.ITEM_ON_HAND)) {
            if (player.getItemOnHand() != null) {
                ioService.write(String.valueOf(player.getItemOnHand().getName()));
            }
            return;
        }
        if (command.getFirstWord().equalsIgnoreCase(CommandSet.ATTACK)) {
            player.attack(player.getUnderAttackUnit(command.getSecondWord()));
            ioService.write(player.getCurrentRoom().getUnitsLifeAsString());
            return;
        }
        if (command.getFirstWord().equalsIgnoreCase(CommandSet.LIFE)) {
            ioService.write(String.valueOf(player.getHealth()));
        }
    }

    public void startConsoleReading() {
        ioService.write(greatMessage());
        Command command;
        while (!(command = new Parser().getCommandFromConsole()).getFirstWord().equalsIgnoreCase(CommandSet.QUIT)
                && Player.getInstance().getHealth() > Player.getInstance().MIN_HEALTH) {
            doCommand(command);
            if(LevelFactory.getOurInstance().getMonsterThreadGroup().activeCount() == 0) {
                break;
            }
        }
        ioService.write(GAME_OVER);
        System.exit(EXIT_CODE);
    }

    private String greatMessage() {
        return GREAT_MESSAGE + CommandSet.getInstance().getAllCommandWords() + "\n";
    }
}
