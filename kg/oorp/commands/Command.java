package kg.oorp.commands;

/**
 * Created by Fujitsu on 23.11.2016.
 */
public class Command {
    private String firstWord;
    private String secondWord;

    public Command(String firstWord, String secondWord) {
        this.firstWord = firstWord;
        this.secondWord = secondWord;
    }

    public String getFirstWord() {
        return firstWord;
    }

    public String getSecondWord() {
        return secondWord;
    }

    public Boolean hasFirstWord() {
        return (null != firstWord);
    }

    public Boolean hasSecondWord() {
        return (null != secondWord);
    }
}
