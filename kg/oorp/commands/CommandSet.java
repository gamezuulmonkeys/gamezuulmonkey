package kg.oorp.commands;


public class CommandSet {
    public static final String GO = "go";
    public static final String HELP = "help";
    public static final String LOOK = "look";
    public static final String ATTACK = "attack";
    public static final String TAKE = "take";
    public static final String DROP = "drop";
    public static final String HEAL = "heal";
    public static final String INVENTORY = "inventory";
    public static final String QUIT = "quit";
    public static final String SELECT_ITEM = "selectItem";
    public static final String ITEM_ON_HAND = "itemOnHand";
    public static final String LIFE = "life";
    private static CommandSet ourInstance = new CommandSet();
    private final String validCommands[] = {
            GO, HELP, LOOK, ATTACK, TAKE, DROP, HEAL, INVENTORY, QUIT, SELECT_ITEM, ITEM_ON_HAND, LIFE
    };

    private CommandSet() {
    }

    public static CommandSet getInstance() {
        return ourInstance;
    }

    public boolean isCommand(String command) {
        for (String element : validCommands) {
            if (null != command && command.equalsIgnoreCase(element)) {
                return true;
            }
        }
        return false;
    }

    public String getAllCommandWords() {
        String commands = "";
        for (String element : validCommands) {
            commands += element + ", ";
        }
        return commands;
    }
}
