package kg.oorp.rooms;


import kg.oorp.items.Item;
import kg.oorp.items.Chest;
import kg.oorp.units.Unit;

import java.util.*;


public class Room {
    public static final Integer NUMBER_OF_ITEMS = 3;
    private String name;
    private Integer keyForRoomValue;
    private Boolean opened;
    private Boolean exit;

    private Set<Item> items;
    private Set<Unit> units;
    private Set<Room> availableRooms;
    private Random random;

    public Room(String name) {
        this.name = name;
        this.opened = true;
        random = new Random();
        items = new HashSet<>();
        units = new HashSet<>();
        availableRooms = new HashSet<>();
        keyForRoomValue = random.nextInt();
        addRandomItems();
    }

    public Boolean isOpened() {
        return opened;
    }

    public void setOpened(Boolean opened) {
        this.opened = opened;
    }

    public Boolean isExit() {
        return exit;
    }

    public void setExit(Boolean exit) {
        this.exit = exit;
    }

    public Integer getKeyValue() {
        return keyForRoomValue;
    }

    public void addAvailableRooms(Room... availableRoom) {
        for (Room room : availableRoom) {
            if (!availableRooms.contains(room)) {
                availableRooms.add(room);
                room.addAvailableRooms(this);
            }
        }
    }

    public Set<Room> getAvailableRooms() {
        return availableRooms;
    }

    public void addUnits(Unit... unit) {
        units.addAll(Arrays.asList(unit));
    }

    public Set<Unit> getUnits() {
        return units;
    }

    public void removeUnit(Unit... unit) {
        units.removeAll(Arrays.asList(unit));
    }

    public void addItems(Item... item) {
        items.addAll(Arrays.asList(item));
    }

    private void addRandomItems() {
        for (int i = 0; i < random.nextInt(NUMBER_OF_ITEMS); i++) {
            items.add(Chest.values()[random.nextInt(Chest.size())]);
        }
    }

    public Set<Item> getItems() {
        return items;
    }

    public void removeItem(Item item) {
        items.remove(item);
    }

    public String getDescription() {
        String description = "";
        for (Unit unit : units) {
            description += "Unit: " + unit.getName() + "\n";
        }
        for (Item item : items) {
            description += "Item: " + item.getName() + "\n";
        }
        for (Room room : availableRooms) {
            description += "Available rooms: " + room.getName() + "\n";
        }
        return "You are in room " + name + " and there are:\n" + description;
    }

    public String getUnitsLifeAsString() {
        String result = "";
        for (Unit unit : units) {
            result += unit.getName() + "\t" + unit.getHealth() + "\n";
        }
        return result;
    }

    public String getName() {
        return name;
    }

}
