package kg.oorp.rooms;

/**
 * Created by Fujitsu on 01.12.2016.
 */
public class RoomFactory {
    private static RoomFactory ourInstance = new RoomFactory();

    private RoomFactory() {
    }

    public static RoomFactory getInstance() {
        return ourInstance;
    }

    public Room getLivingRoom() {
        return new Room("LivingRoom");
    }

    public Room getKitchen() {
        return new Room("Kitchen");
    }

    public Room getBedroom() {
        return new Room("Bedroom");
    }

    public Room getHall() {
        return new Room("Hall");
    }

    public Room getBathroom() {
        return new Room("Bathroom");
    }

    public Room getCanteen() {
        return new Room("Canteen");
    }

    public Room getCabinet() {
        return new Room("Cabinet");
    }

    public Room getCellar() {
        return new Room("Cellar");
    }
}
